# Map-server

[![go Version](https://img.shields.io/badge/Go-v1.18-blue)](v1.16.6)


地图下载器web的服务端



## Features
Gin框架
协程管理
地图添加
## Quick start
1. 安装Go-bindata
```
    go get github.com/go-bindata/go-bindata/...
    go get github.com/elazarl/go-bindata-assetfs/...
    go >=1.17
    go install -a -v github.com/go-bindata/go-bindata/...@latest
    go install -a -v github.com/elazarl/go-bindata-assetfs/...
```
2. 打包静态文件资源build
```
    go-bindata-assetfs -o asset.go -pkg asset ./build/...
```
3. 配置config.yaml

#### Requirements

- Go version  1.18
- Global environment configure (Linux/Mac)

```
export GO111MODULE=on
export GOPROXY=https://goproxy.io
```

#### Build & Run

```
go run cmd/svr/main.go
go build cmd/svr/main.go
```

